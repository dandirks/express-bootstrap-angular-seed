/* jshint globalstrict: true */

'use strict';

// Declare app level module
angular.module('app', ['app.controllers', 'app.services', 'app.directives']).
  config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider.when('/', {templateUrl: 'templates/partials/home.html', controller: 'HomeCtrl'});
    $routeProvider.when('/view2', {templateUrl: 'templates/partials/partial2.html', controller: 'MyCtrl2'});
    $routeProvider.otherwise({redirectTo: '/'});

    $locationProvider.html5Mode(true);
  }]);
