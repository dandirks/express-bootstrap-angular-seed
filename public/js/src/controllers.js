/* Controllers */

angular.module('app.controllers', []).
  controller('HomeCtrl', ['$scope', function($scope) {
    $scope.title = 'Home';
  }]).
  controller('MyCtrl2', ['$scope', function($scope) {
    $scope.title = 'MyCtrl2';
  }]);
