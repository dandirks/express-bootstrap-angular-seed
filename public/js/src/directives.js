/* Directives */

angular.module('app.directives', []).
  directive('activeWhenRoute', ['$location', function(location) {
    return {
      restrict: 'A',
      link: function(scope, iElement, iAttrs) {
        var path = iAttrs.activeWhenRoute;
        scope.location = location;
        scope.$watch('location.path()', function(newPath) {
          iElement[(path === newPath) ? 'addClass' : 'removeClass']('active');
        });
      }
    };
  }]);