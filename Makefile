ANGULAR_VERSION = 1.1.5
BOOTSTRAP_VERSION = 2.3.1

build: bootstrap-css .angular/.angular$(ANGULAR_VERSION)
	@./node_modules/.bin/grunt

build_watch:
	@./node_modules/.bin/grunt build:watch

frontend_unit_tests:
	@./node_modules/.bin/karma start --single-run

frontend_unit_tests_watch:
	@./node_modules/.bin/karma start

js: .angular/.angular$(ANGULAR_VERSION)
	@./node_modules/.bin/grunt js

css: bootstrap-css
	@./node_modules/.bin/grunt css

bootstrap-css: .bootstrap/.bootstrap$(BOOTSTRAP_VERSION)
ifdef BOOTSTRAP_VERSION
	@echo "Building Boostrap CSS v$(BOOTSTRAP_VERSION)..."
	@cp public/css/src/variables.less .bootstrap/less/
	@mkdir -p public/css/lib/bootstrap/
	@cd .bootstrap; make bootstrap-css > /dev/null; cp bootstrap/css/bootstrap*.min.css ../public/css/lib/bootstrap/
endif

.angular/.angular$(ANGULAR_VERSION): .angular
ifdef ANGULAR_VERSION
	@echo "Building Angular JS v$(ANGULAR_VERSION)..."
	@rm -rf .angular/.angular*
	@rm -rf .angular/build
	@mkdir -p public/js/lib/angular/
	@cd .angular; git checkout -b $(ANGULAR_VERSION) v$(ANGULAR_VERSION); npm install; ../node_modules/.bin/grunt buildall minall
	@cd .angular/build; ls . | grep -Ev '(bootstrap|scenario|mocks)' | xargs cp -t ../../public/js/lib/angular/; cp angular-mocks.js ../../test/frontend/lib/
	@cd .angular; git checkout master; git branch -D $(ANGULAR_VERSION)
	@touch .angular/.angular$(ANGULAR_VERSION)
endif

.angular:
ifdef ANGULAR_VERSION
	@git clone https://github.com/angular/angular.js.git .angular
endif

.bootstrap/.bootstrap$(BOOTSTRAP_VERSION): .bootstrap
ifdef BOOTSTRAP_VERSION
	@rm -rf .bootstrap/.bootstrap*
	@cd .bootstrap; git checkout master; git checkout v$(BOOTSTRAP_VERSION); npm install
	@touch .bootstrap/.bootstrap$(BOOTSTRAP_VERSION)
endif

.bootstrap:
ifdef BOOTSTRAP_VERSION
	@git clone https://github.com/twitter/bootstrap.git .bootstrap
endif
