module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        sourceMap: 'public/js/app.map',
        sourceMappingURL: '/js/app.map',
        sourceMapPrefix: 2
      },
      build: {
        src: [
          'public/js/src/app.js',
          'public/js/src/controllers.js',
          'public/js/src/directives.js',
          'public/js/src/filters.js',
          'public/js/src/services.js',
        ],
        dest: 'public/js/app.js'
      }
    },
    less: {
      production: {
        options: {
          yuicompress: true
        },
        files: {
          "public/css/app.css": "public/css/src/app.less"
        }
      }
    },
    watch: {
      js: {
        files: ['public/js/src/*.js'],
        tasks: ['jshint:frontend', 'uglify'],
        options: {
          nospawn: true
        }
      },
      css: {
        files: ['public/css/src/*.less'],
        tasks: ['less']
      }

    },
    jshint: {
      frontend: {
        files: {
          src: 'public/js/src/*.js'
        },
        options: { browser: true, globals: { angular: false } }
      },
      options: {
        quotmark: 'single',
        bitwise: true,
        indent: 2,
        camelcase: true,
        strict: false,
        trailing: true,
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noempty: true,
        unused: true,
        noarg: true,
        sub: true,
        undef: true,
        maxdepth: 4,
        maxlen: 120,
        globals: {}
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-jshint');

  // Default task(s).
  grunt.registerTask('default', ['jshint', 'uglify', 'less']);
  grunt.registerTask('js', ['uglify']);
  grunt.registerTask('css', ['less']);
  grunt.registerTask('build:watch', ['watch']);

  grunt.event.on('watch', function(action, filepath) {
    grunt.config(['jshint', 'frontend', 'files', 'src'], filepath);
  });

};